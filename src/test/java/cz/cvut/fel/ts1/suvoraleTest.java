package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class suvoraleTest {
    @Test
    public void factorialTest(){
        suvorale suvorale = new suvorale();
        long res1 = suvorale.factorial(1);
        long res2 = suvorale.factorial(3);
        Assertions.assertEquals(1, res1);
        Assertions.assertEquals(6, res2);
    }
}

